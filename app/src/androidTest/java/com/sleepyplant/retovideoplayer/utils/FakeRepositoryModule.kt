package com.sleepyplant.retovideoplayer.utils

import android.provider.MediaStore
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sleepyplant.retovideoplayer.data.common.utils.WrappedResponse
import com.sleepyplant.retovideoplayer.data.course.CourseModule
import com.sleepyplant.retovideoplayer.data.course.remote.dto.CourseResponse
import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse
import com.sleepyplant.retovideoplayer.domain.common.base.BaseResult
import com.sleepyplant.retovideoplayer.domain.course.CourseRepository
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Singleton


@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [CourseModule::class]
)

class FakeRepositoryModule {

    @Provides
    @Singleton
    fun providerCourseRepository(): CourseRepository =
        object : CourseRepository {



            override suspend fun getCourseById(id: String): Flow<BaseResult<CourseEntity, WrappedResponse<CourseResponse>>> = flow {

                val course = CourseEntity(
                    1,
                    "Angular de 0 a experto",
                    listOf(
                        VideoResponse(
                            1,
                            "1. Introducción",
                            "https://crehana-videos.akamaized.net/outputs/trailer/89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8",
                            "1",
                            "2021-10-18 14:57:45",
                            "2021-10-18 14:57:45"
                            ),
                        VideoResponse(
                            1,
                            "2. Typescripy y NodeJS",
                            "https://crehana-videos.akamaized.net/outputs/trailer/29f6759c6bd04a5f9cdd4e351fc3ddde/d0da0c55b987479fafd7fb68ca298e5f.m3u8",
                            "1",
                            "2021-10-18 14:57:45",
                            "2021-10-18 14:57:45"
                        )
                    ))
                emit(BaseResult.Success(course))

            }

        }
}