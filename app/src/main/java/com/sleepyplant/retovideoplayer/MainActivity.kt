package com.sleepyplant.retovideoplayer

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.content.res.Resources
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse
import com.sleepyplant.retovideoplayer.databinding.ActivityMainBinding
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import com.sleepyplant.retovideoplayer.presentation.CourseViewModel
import com.sleepyplant.retovideoplayer.presentation.VideosAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector





@AndroidEntryPoint
class MainActivity : AppCompatActivity(){

    private lateinit var constraintLayoutRoot: ConstraintLayout
    private lateinit var exoPlayerView: PlayerView
    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var mediaSource:MediaSource
    private lateinit var fullscreenBtn: AppCompatImageView
    private lateinit var optionsBtn: AppCompatImageView
    private lateinit var titleText: TextView
    private val viewModel:CourseViewModel by viewModels()
    lateinit var binding: ActivityMainBinding
    var defaultVideoID = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fullscreenBtn = findViewById(R.id.bt_fullscreen)
        optionsBtn = findViewById(R.id.bt_options)
        titleText = findViewById(R.id.video_title)
        titleText.visibility = View.INVISIBLE
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideSystemUI()
        }

        findView()
        initPlayer()

        fullscreenBtn.setOnClickListener{
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                showSysteUI()
            }else{
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                hideSystemUI()
            }
        }

        optionsBtn.setOnClickListener{
            var parameters : Bundle = Bundle()
            parameters.putString("resolution", "1920")
            parameters.putString("speed", "x1")

            var videoOptions:VideoOptions = VideoOptions()
            videoOptions.arguments = parameters

            videoOptions.setItemTapListener(object : VideoOptions.OnItemTapSpeed{
                override fun onTap(type:TypeSetup, index: Int) {
                    if(type==TypeSetup.SPEED){
                        var speed : Float = 1f
                        speed = when(index){
                            0 -> 0.25f
                            1 -> 0.5f
                            3 -> 0.75f
                            else -> 1f
                        }

                        var param: PlaybackParameters  = PlaybackParameters(speed)
                        simpleExoPlayer.playbackParameters = param

                    }else if(type==TypeSetup.RESOLUTION){
                        //simpleExoPlayer.tra
                        //Log.i("track selector", simpleExoPlayer.currentTrackGroups.toString()+" "+simpleExoPlayer.currentTrackGroups.length+"  "+simpleExoPlayer.trackSelector.toString())
                        //trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(320,180).setMaxVideoBitrate(1000))
                        //simpleExoPlayer = SimpleExoPlayer.Builder(baseContext).setTrackSelector(trackSelector).build()
                        reloadPlayer(index)


                    }


                }
            })

            loadFragment(videoOptions)
        }


        observeCourse()
        fetchFirstCourse()
        setupRecyclerView()

    }

    private fun setupRecyclerView(){
        val mAdapter = VideosAdapter(mutableListOf())
        mAdapter.setItemTapListener(object : VideosAdapter.OnItemTap{
            override fun onTap(video: VideoResponse) {
                createMediaSource(video.url)
            }
        })

        binding.rvVideos?.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(this.context)
        }
    }


    private fun loadFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        //transaction.add(R.id.optionsContainer, fragment, bundle)
        transaction.replace(R.id.optionsContainer, fragment)
        transaction.disallowAddToBackStack()
        transaction.commit()
    }

    private fun observeCourse(){
        viewModel.course.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { course ->
                course?.let { handleCourse(it) }
            }
            .launchIn(lifecycleScope)
    }

    private fun fetchFirstCourse(){
        val id = 1
        if (id != 0){
            viewModel.getCourseById(id.toString())
        }
    }

    private fun handleCourse(courseEntity: CourseEntity){
        titleText.text = courseEntity.videos[defaultVideoID].title

        val dataSourceFactory : DataSource.Factory = DefaultDataSourceFactory(
            this, Util.getUserAgent(this, applicationInfo.name)
        )

        mediaSource = HlsMediaSource.Factory(dataSourceFactory).createMediaSource(
            MediaItem.fromUri(Uri.parse(courseEntity.videos[defaultVideoID].url))
        )

        simpleExoPlayer.setMediaSource(mediaSource)
        simpleExoPlayer.prepare()


        binding.videoTitle?.text = courseEntity.name


        binding.rvVideos?.adapter?.let {
            if(it is VideosAdapter){
                it.updateList(courseEntity.videos)
            }

        }

    }

    private fun findView(){
        constraintLayoutRoot = findViewById(R.id.constraintLayoutRoot)
        exoPlayerView = findViewById(R.id.exoPlayerView)
    }

    private lateinit var trackSelector : DefaultTrackSelector

    private fun reloadPlayer(withResolution:Int){
        trackSelector = DefaultTrackSelector(this)

        if(withResolution==0){
            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(1920,1080).setMaxVideoBitrate(5000))
        }else if(withResolution==1){
            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(1280,720).setMaxVideoBitrate(2500))
        }else if(withResolution==2){
            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(640,480).setMaxVideoBitrate(1500))
        }else if(withResolution==3){
            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(480,360).setMaxVideoBitrate(100))
        }else{
            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(1920,1080).setMaxVideoBitrate(5000))
        }

        simpleExoPlayer.prepare()

    }

    private fun initPlayer(){
        trackSelector = DefaultTrackSelector(this)
        //trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSize(180,90).setMaxVideoBitrate(500))
        simpleExoPlayer = SimpleExoPlayer.Builder(this).setTrackSelector(trackSelector).build()
        exoPlayerView.player = simpleExoPlayer
        simpleExoPlayer.addListener(playerListener)
    }

    private fun createMediaSource(url:String){
        val hls = url
        val dataSourceFactory : DataSource.Factory = DefaultDataSourceFactory(
            this, Util.getUserAgent(this, applicationInfo.name)
        )

        mediaSource = HlsMediaSource.Factory(dataSourceFactory).createMediaSource(
            MediaItem.fromUri(Uri.parse(hls))
        )

        simpleExoPlayer.setMediaSource(mediaSource)
        simpleExoPlayer.prepare()

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            hideSystemUI()
        }else{
            showSysteUI()
        }

        window.decorView.requestLayout()

    }

    fun Int.toDP():Int = (this/ Resources.getSystem().displayMetrics.density).toInt()
    private fun hideSystemUI(){
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                )
    }

    private fun showSysteUI(){
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                )
    }

    override fun onPause() {
        super.onPause()
        simpleExoPlayer.pause()
        simpleExoPlayer.playWhenReady = false
    }

    override fun onResume() {
        super.onResume()
        simpleExoPlayer.playWhenReady = true
        simpleExoPlayer.play()

        var sender = intent.getStringExtra("SENDER_KEY").toString()
        Log.i("bh", "intend $sender")

    }

    override fun onStop() {
        super.onStop()
        simpleExoPlayer.pause()
        simpleExoPlayer.playWhenReady = false
    }

    override fun onDestroy() {
        super.onDestroy()
        simpleExoPlayer.removeListener(playerListener)
        simpleExoPlayer.stop()
        simpleExoPlayer.clearMediaItems()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private var playerListener = object : Player.Listener{

        override fun onRenderedFirstFrame() {
            super.onRenderedFirstFrame()
        }

        override fun onPlayerError(error: PlaybackException) {
            super.onPlayerError(error)
            Toast.makeText(this@MainActivity, "${error.message}", Toast.LENGTH_SHORT).show()
        }

    }


}