package com.sleepyplant.retovideoplayer

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.sleepyplant.retovideoplayer.databinding.FragmentVideoOptionsBinding
import dagger.hilt.android.AndroidEntryPoint
import android.content.Intent
import android.util.Log
import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse
import com.sleepyplant.retovideoplayer.presentation.VideosAdapter


@AndroidEntryPoint
class VideoOptions : Fragment(), AdapterView.OnItemClickListener {

    private var listView: ListView ? = null
    private var arrayAdapterQualities: ArrayAdapter<String> ? = null
    private var arrayAdapterSpeeds: ArrayAdapter<String> ? = null
    private var arrayAdapterSubtitles: ArrayAdapter<String> ? = null

    private lateinit var binding: FragmentVideoOptionsBinding




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentVideoOptionsBinding.inflate(inflater, container, false)

        arrayAdapterQualities =
            activity?.let { ArrayAdapter(it, R.layout.select_dialog_single_choice_mod, resources.getStringArray(R.array.videoQualities)) }

        binding.qualities.adapter = arrayAdapterQualities
        binding.qualities.onItemClickListener = this

        arrayAdapterSpeeds =
            activity?.let { ArrayAdapter(it, R.layout.select_dialog_single_choice_mod, resources.getStringArray(R.array.videoSpeed)) }

        binding.speeds.adapter = arrayAdapterSpeeds
        binding.speeds.onItemClickListener = this


        arrayAdapterSubtitles =
            activity?.let { ArrayAdapter(it, R.layout.select_dialog_single_choice_mod, resources.getStringArray(R.array.subtitles)) }

        binding.subtitles?.adapter = arrayAdapterSubtitles
        binding.subtitles?.onItemClickListener = this


        binding.closeVideoOptions.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
        }

        return binding.root


    }

    interface OnItemTapSpeed {
        fun onTap(type:TypeSetup, index: Int)
    }

    fun setItemTapListener(l: VideoOptions.OnItemTapSpeed){
        onTapListener = l
    }

    private var onTapListener: VideoOptions.OnItemTapSpeed? = null


    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

        var items:String = p0?.getItemAtPosition(p2) as String

        if(items.contains("x")){
            onTapListener?.onTap(TypeSetup.SPEED, p2)
        }else{
            onTapListener?.onTap(TypeSetup.RESOLUTION, p2)
        }

    }


}

enum class TypeSetup {
    SPEED, RESOLUTION, SUBTITLES
}