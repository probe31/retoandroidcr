package com.sleepyplant.retovideoplayer.data.course

import com.sleepyplant.retovideoplayer.data.common.module.NetworkModule
import com.sleepyplant.retovideoplayer.data.course.remote.api.CourseApi
import com.sleepyplant.retovideoplayer.data.course.repository.CourseRepositoryImpl
import com.sleepyplant.retovideoplayer.domain.course.CourseRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
class CourseModule {
    @Singleton
    @Provides
    fun provideProductApi(retrofit: Retrofit) : CourseApi {
        return retrofit.create(CourseApi::class.java)
    }

    @Singleton
    @Provides
    fun provideProductRepository(courseApi: CourseApi) : CourseRepository {
        return CourseRepositoryImpl(courseApi)
    }
}