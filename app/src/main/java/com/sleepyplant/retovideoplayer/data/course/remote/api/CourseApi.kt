package com.sleepyplant.retovideoplayer.data.course.remote.api

import com.sleepyplant.retovideoplayer.data.common.utils.WrappedResponse
import com.sleepyplant.retovideoplayer.data.course.remote.dto.CourseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface CourseApi {
    @GET("courses/{id}")
    suspend fun getCourseById(@Path("id") id: String) : Response<WrappedResponse<CourseResponse>>
}