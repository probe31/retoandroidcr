package com.sleepyplant.retovideoplayer.data.course.remote.dto


import com.google.gson.annotations.SerializedName
import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse

data class CourseResponse (
    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("created_at") var createdAt: String,
    @SerializedName("updated_at") var updatedAt: String,
    @SerializedName("videos") var videos: List<VideoResponse>
)