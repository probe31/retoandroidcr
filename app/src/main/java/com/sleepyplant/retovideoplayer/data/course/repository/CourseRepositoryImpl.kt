package com.sleepyplant.retovideoplayer.data.course.repository

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sleepyplant.retovideoplayer.data.common.utils.WrappedResponse
import com.sleepyplant.retovideoplayer.data.course.remote.api.CourseApi
import com.sleepyplant.retovideoplayer.data.course.remote.dto.CourseResponse
import com.sleepyplant.retovideoplayer.domain.common.base.BaseResult
import com.sleepyplant.retovideoplayer.domain.course.CourseRepository
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CourseRepositoryImpl @Inject constructor(private val courseApi: CourseApi) : CourseRepository {

    override suspend fun getCourseById(id: String): Flow<BaseResult<CourseEntity, WrappedResponse<CourseResponse>>> {
        return flow {
            val response = courseApi.getCourseById(id)

            if(response.isSuccessful){
                val body = response.body()!!
                val course = CourseEntity(body.data?.id!!, body.data?.name!!,body.data?.videos!!)
                emit(BaseResult.Success(course))
            }else{
                val type = object : TypeToken<WrappedResponse<CourseResponse>>(){}.type
                val err = Gson().fromJson<WrappedResponse<CourseResponse>>(response.errorBody()!!.charStream(), type)!!
                err.code = response.code()
                emit(BaseResult.Error(err))
            }
        }
    }


}