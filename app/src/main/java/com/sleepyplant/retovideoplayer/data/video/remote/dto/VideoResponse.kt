package com.sleepyplant.retovideoplayer.data.video.remote.dto

import com.google.gson.annotations.SerializedName

data class VideoResponse (
    @SerializedName("id") var id: Int,
    @SerializedName("title") var title: String,
    @SerializedName("url") var url: String,
    @SerializedName("course_id") var course_id: String,
    @SerializedName("created_at") var created_at: String,
    @SerializedName("updated_at") var updated_at: String
)