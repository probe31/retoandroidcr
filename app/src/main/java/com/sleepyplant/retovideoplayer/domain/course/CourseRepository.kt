package com.sleepyplant.retovideoplayer.domain.course

import com.sleepyplant.retovideoplayer.data.common.utils.WrappedResponse
import com.sleepyplant.retovideoplayer.data.course.remote.dto.CourseResponse
import com.sleepyplant.retovideoplayer.domain.common.base.BaseResult
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import kotlinx.coroutines.flow.Flow

interface CourseRepository {

    suspend fun getCourseById(id: String) : Flow<BaseResult<CourseEntity, WrappedResponse<CourseResponse>>>

}