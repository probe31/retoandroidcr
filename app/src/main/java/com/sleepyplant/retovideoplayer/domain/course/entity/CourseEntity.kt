package com.sleepyplant.retovideoplayer.domain.course.entity

import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse
import com.sleepyplant.retovideoplayer.domain.video.entity.VideoEntity

data class CourseEntity (
    var id: Int,
    var name: String,
    var videos: List<VideoResponse>
)