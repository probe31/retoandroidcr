package com.sleepyplant.retovideoplayer.domain.course.usercase

import com.sleepyplant.retovideoplayer.data.common.utils.WrappedResponse
import com.sleepyplant.retovideoplayer.data.course.remote.dto.CourseResponse
import com.sleepyplant.retovideoplayer.domain.common.base.BaseResult
import com.sleepyplant.retovideoplayer.domain.course.CourseRepository
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCourseByIdUseCase  @Inject constructor(private val courseRepository: CourseRepository){
    suspend fun invoke(id: String) : Flow<BaseResult<CourseEntity, WrappedResponse<CourseResponse>>> {
        return courseRepository.getCourseById(id)
    }
}