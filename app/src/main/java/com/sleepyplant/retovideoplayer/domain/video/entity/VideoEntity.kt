package com.sleepyplant.retovideoplayer.domain.video.entity

data class VideoEntity (
    var id: Int,
    var title: String,
    var url: String,
    var courseId: String
)