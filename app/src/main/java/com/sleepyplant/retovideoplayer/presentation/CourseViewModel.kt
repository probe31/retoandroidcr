package com.sleepyplant.retovideoplayer.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sleepyplant.retovideoplayer.domain.common.base.BaseResult
import com.sleepyplant.retovideoplayer.domain.course.entity.CourseEntity
import com.sleepyplant.retovideoplayer.domain.course.usercase.GetCourseByIdUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CourseViewModel @Inject constructor(private val getCourseByIdUseCase: GetCourseByIdUseCase) : ViewModel(){

    private val _course = MutableStateFlow<CourseEntity?>(null)
    val course : StateFlow<CourseEntity?> get() = _course

    fun getCourseById(id: String){
        viewModelScope.launch {
            getCourseByIdUseCase.invoke(id)
                .onStart {
                    //setLoading()
                }
                .catch { exception ->
                    //hideLoading()
                    //showToast(exception.stackTraceToString())
                }
                .collect { result ->
                    //hideLoading()
                    when(result){
                        is BaseResult.Success -> {
                            _course.value = result.data
                        }
                        is BaseResult.Error -> {
                            //showToast(result.rawResponse.message)
                        }
                    }
                }
        }
    }


}