package com.sleepyplant.retovideoplayer.presentation

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sleepyplant.retovideoplayer.R
import com.sleepyplant.retovideoplayer.data.video.remote.dto.VideoResponse
import com.sleepyplant.retovideoplayer.databinding.ItemVideoCourseBinding
import com.sleepyplant.retovideoplayer.domain.video.entity.VideoEntity

class VideosAdapter (private val videos:MutableList<VideoResponse>):RecyclerView.Adapter<VideosAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = ItemVideoCourseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(videos[position])

    override fun getItemCount(): Int = videos.size

    inner class ViewHolder(private val itemBinding: ItemVideoCourseBinding) : RecyclerView.ViewHolder(itemBinding.root){
        fun bind(video: VideoResponse){
            itemBinding.titleVideo.text = video.title
            itemBinding.root.setOnClickListener {
                onTapListener?.onTap(video)
            }
        }
    }


    fun updateList(mVideos: List<VideoResponse>){
        videos.clear()
        videos.addAll(mVideos)

        notifyDataSetChanged()
    }

    interface OnItemTap {
        fun onTap(product: VideoResponse)
    }

    fun setItemTapListener(l: OnItemTap){
        onTapListener = l
    }

    private var onTapListener: OnItemTap? = null

}